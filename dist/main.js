"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function hello(compiler) {
    console.log("Hello from ".concat(compiler));
}
hello("TypeScript");
var greet_1 = require("./greet");
console.log((0, greet_1.sayHello)("TypeScript 123"));
var ts = require("typescript");
function compile(fileNames, options) {
    console.log('fileNames=' + fileNames);
    var program = ts.createProgram(fileNames, options);
    var emitResult = program.emit();
    var allDiagnostics = ts
        .getPreEmitDiagnostics(program)
        .concat(emitResult.diagnostics);
    allDiagnostics.forEach(function (diagnostic) {
        if (diagnostic.file) {
            var _a = ts.getLineAndCharacterOfPosition(diagnostic.file, diagnostic.start), line = _a.line, character = _a.character;
            var message = ts.flattenDiagnosticMessageText(diagnostic.messageText, "\n");
            console.log("msg=".concat(diagnostic.file.fileName, " (").concat(line + 1, ",").concat(character + 1, "): ").concat(message));
        }
        else {
            console.log('msg=' + ts.flattenDiagnosticMessageText(diagnostic.messageText, "\n"));
        }
    });
    var exitCode = emitResult.emitSkipped ? 1 : 0;
    console.log("Process exiting with code '".concat(exitCode, "'."));
    process.exit(exitCode);
}
compile(process.argv.slice(2), {
    noEmitOnError: true,
    noImplicitAny: true,
    target: ts.ScriptTarget.ES5,
    module: ts.ModuleKind.CommonJS
});

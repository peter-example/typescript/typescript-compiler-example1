function hello(compiler: string) {
  console.log(`Hello from ${compiler}`);
}
hello("TypeScript");

import { sayHello } from "./greet";
console.log(sayHello("TypeScript 123"));

import * as ts from "typescript";

function compile(fileNames: string[], options: ts.CompilerOptions): void {
  console.log('fileNames=' + fileNames);
  let program = ts.createProgram(fileNames, options);
  let emitResult = program.emit();

  let allDiagnostics = ts
    .getPreEmitDiagnostics(program)
    .concat(emitResult.diagnostics);

  allDiagnostics.forEach(diagnostic => {
    if (diagnostic.file) {
      let { line, character } = ts.getLineAndCharacterOfPosition(diagnostic.file, diagnostic.start!);
      let message = ts.flattenDiagnosticMessageText(diagnostic.messageText, "\n");
      console.log(`msg=${diagnostic.file.fileName} (${line + 1},${character + 1}): ${message}`);
    } else {
      console.log('msg=' + ts.flattenDiagnosticMessageText(diagnostic.messageText, "\n"));
    }
  });

  let exitCode = emitResult.emitSkipped ? 1 : 0;
  console.log(`Process exiting with code '${exitCode}'.`);
  process.exit(exitCode);
}

compile(process.argv.slice(2), {
  noEmitOnError: true,
  noImplicitAny: true,
  target: ts.ScriptTarget.ES5,
  module: ts.ModuleKind.CommonJS
});